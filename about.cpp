#include "about.h"
#include "ui_about.h"
#include "qicon.h"

About::About(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::About)
{
    ui->setupUi(this);



    QPixmap pixmap(":/png/logo.png");

    ui->label->setAlignment(Qt::AlignCenter);

    pixmap = pixmap.scaled(ui->label->size(),Qt::KeepAspectRatio);

    ui->label->setPixmap( pixmap );
}

About::~About()
{
    delete ui;
}

void About::on_pushButton_clicked()
{
    close();
}

