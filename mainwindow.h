#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSystemTrayIcon>
#include <QMenu>
#include <QComboBox>
#include <QFileSystemModel>
#include <QTreeView>
#include "about.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; class About; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

private slots:
    void setIcon(int index);
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

    void on_actionAbout_triggered();

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QSystemTrayIcon *tray;
    QMenu *trayIconMenu;
    QComboBox *iconComboBox;
    QFileSystemModel *fileModel;

    QString devboxDir;
    QString proccessString;

    void proc(QString command, QStringList arguments, QString workDir);

    void start();
    void stop();
    void openDataFolder();
    void createDevilbox();
    void openDir();
    void about();

};
#endif // MAINWINDOW_H
