#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "qfiledialog.h"
#include <qmessagebox.h>
#include <qprocess.h>
#include "about.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setIcon(int index)
{
    QIcon icon = iconComboBox->itemIcon(index);
    tray->setIcon(icon);
    setWindowIcon(icon);

    tray->setToolTip(iconComboBox->itemText(index));
}



void MainWindow::proc(QString command, QStringList arguments, QString workDir)
{
    QProcess *myProcess = new QProcess();

    myProcess->setProcessChannelMode(QProcess::MergedChannels);
    myProcess->setWorkingDirectory(workDir);
    myProcess->start(command, arguments);
    myProcess->waitForFinished();

    proccessString.append(myProcess->readAllStandardOutput());

    ui->textEdit->setText(proccessString);
}

void MainWindow::start()
{
    QStringList arguments;
    arguments << "bash";
    arguments << "./start.sh";

    proc("sudo", arguments, devboxDir);
}

void MainWindow::stop()
{
    QStringList arguments;
    arguments << "bash";
    arguments << "./stop.sh";

    proc("sudo", arguments, devboxDir);
}

void MainWindow::openDir()
{
    devboxDir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                "/home",
                                                QFileDialog::ShowDirsOnly
                                                | QFileDialog::DontResolveSymlinks);

    ui->pushButton->setDisabled(true);
    ui->pushButton->setText(devboxDir);
    ui->pushButton_5->setDisabled(true);
}

void MainWindow::openDataFolder()
{
    QStringList arguments;
    arguments << devboxDir+"/data/www/";

    proc("xdg-open", arguments, devboxDir);
}

void MainWindow::createDevilbox()
{
    QStringList arguments;
    arguments << "clone" << "https://gitlab.com/onix-os/applications/devilbox.git" << "devilbox";

    QString workDir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                    "/home",
                                                    QFileDialog::ShowDirsOnly
                                                    | QFileDialog::DontResolveSymlinks);


    devboxDir = workDir+"/devilbox";
    proc("git", arguments, workDir);

    QStringList updateArgs;
    updateArgs << "env-example" << ".env";
    proc("cp", updateArgs, devboxDir);


    ui->pushButton->setDisabled(true);
    ui->pushButton->setText(devboxDir);
    ui->pushButton_5->setDisabled(true);
}

void MainWindow::about()
{
    About *about = new About();
    about->show();
}

void MainWindow::on_pushButton_5_clicked()
{
    createDevilbox();
}


void MainWindow::on_pushButton_clicked()
{
    openDir();
}


void MainWindow::on_pushButton_4_clicked()
{
    openDataFolder();
}


void MainWindow::on_pushButton_3_clicked()
{
    stop();
}


void MainWindow::on_pushButton_2_clicked()
{
    start();
}


void MainWindow::on_actionAbout_triggered()
{
    about();
}

